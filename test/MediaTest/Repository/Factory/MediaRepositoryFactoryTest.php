<?php
namespace MediaTest\Repository\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Interop\Container\ContainerInterface;
use Media\Repository\MediaRepository;
use Media\Repository\Factory\MediaRepositoryFactory;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Test for the MediaRepositoryFactory
 * @author heik
 * @covers Media\Repository\Factory\MediaRepositoryFactory
 */
class MediaRepositoryFactoryTest extends TestCase
{
    public function testCreateService()
    {
        $entityManager = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $containerMap = [
            ['doctrine.entitymanager.orm_default', $entityManager]
        ];

        $container = $this->getMockBuilder(ContainerInterface::class)->getMock();
        $container->method('get')->willReturnMap($containerMap);

        $factory = new MediaRepositoryFactory();
        $this->assertInstanceOf(FactoryInterface::class, $factory);

        $result = $factory->__invoke($container, 'media-repository');
        $this->assertInstanceOf(MediaRepository::class, $result);
    }
}
