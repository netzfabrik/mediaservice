<?php
namespace MediaTest\Repository;

use PHPUnit\Framework\TestCase;
use Media\Repository\MediaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Media\Entity\Media;

/**
 * Tests for the MediaRepository
 * @author heik
 * @covers Media\Repository\MediaRepository
 */
class MediaRepositoryTest extends TestCase
{
    /**
     * Test findMediaByIdAndSize
     */
    public function testFindMediaByIdAndSizeXY()
    {
        $mediaId = 100;
        $x = 500;
        $y = 400;

        $repo = $this->getMockBuilder(MediaRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findOneBy'])
            ->getMock();

        $repo->expects($this->once())->method('findOneBy')->with(['source' => $mediaId, 'xScale' => $x, 'yScale' => $y])->willReturn('result');
        $this->assertEquals('result', $repo->findMediaByIdAndSize($mediaId, $x, $y));
    }

    /**
     * Test findMediaByIdAndSize
     */
    public function testFindMediaByIdAndSizeX()
    {
        $mediaId = 100;
        $x = 500;

        $repo = $this->getMockBuilder(MediaRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findOneBy'])
            ->getMock();

        $repo->expects($this->once())->method('findOneBy')->with(['source' => $mediaId, 'xScale' => $x])->willReturn('result');
        $this->assertEquals('result', $repo->findMediaByIdAndSize($mediaId, $x));
    }

    /**
     * Test findMediaByIdAndSize
     */
    public function testFindMediaByIdAndSizeY()
    {
        $mediaId = 100;
        $y = 500;

        $repo = $this->getMockBuilder(MediaRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['findOneBy'])
            ->getMock();

        $repo->expects($this->once())->method('findOneBy')->with(['source' => $mediaId, 'yScale' => $y])->willReturn('result');
        $this->assertEquals('result', $repo->findMediaByIdAndSize($mediaId, null, $y));
    }

    /**
     * Test findMediaByIdAndSize
     */
    public function testFindMediaByIdAndSize()
    {
        $mediaId = 100;
        $repo = $this->getMockBuilder(MediaRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['find'])
            ->getMock();

        $repo->expects($this->once())->method('find')->with($mediaId)->willReturn('result');
        $this->assertEquals('result', $repo->findMediaByIdAndSize($mediaId));
    }

    /**
     * Test remove entity
     */
    public function testRemove()
    {
        $media = $this->getMockBuilder(Media::class)->getMock();

        $em = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $em->expects($this->once())->method('remove')->with($media);
        $em->expects($this->once())->method('flush')->with($media);

        $repo = $this->getMockBuilder(MediaRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['getEntityManager'])
            ->getMock();
        $repo->expects($this->once())->method('getEntityManager')->willReturn($em);

        $this->assertNull($repo->remove($media));
    }

    /**
     * Test persist entity with flush
     */
    public function testPersist()
    {
        $media = $this->getMockBuilder(Media::class)->getMock();

        $em = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $em->expects($this->once())->method('persist')->with($media);
        $em->expects($this->once())->method('flush')->with($media);

        $repo = $this->getMockBuilder(MediaRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['getEntityManager'])
            ->getMock();
        $repo->expects($this->once())->method('getEntityManager')->willReturn($em);

        $this->assertSame($media, $repo->persist($media, true));
    }

    /**
     * Test persist entity without flush
     */
    public function testPersistWithoutFlush()
    {
        $media = $this->getMockBuilder(Media::class)->getMock();

        $em = $this->getMockBuilder(EntityManagerInterface::class)->getMock();
        $em->expects($this->once())->method('persist')->with($media);
        $em->expects($this->never())->method('flush');

        $repo = $this->getMockBuilder(MediaRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(['getEntityManager'])
            ->getMock();
        $repo->expects($this->once())->method('getEntityManager')->willReturn($em);

        $this->assertSame($media, $repo->persist($media, false));
    }
}
