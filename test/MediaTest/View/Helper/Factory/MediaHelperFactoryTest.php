<?php
namespace MediaTest\View\Helper\Factory;

use Interop\Container\ContainerInterface;
use Media\Service\MediaService;
use Media\View\Helper\MediaHelper;
use Media\View\Helper\Factory\MediaHelperFactory;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Tests for the MediaHelperFactory
 * @author heik
 * @covers Media\View\Helper\Factory\MediaHelperFactory
 */
class MediaHelperFactoryTest extends TestCase
{
    /**
     * Test interface and service creation
     */
    public function testCreateService()
    {
        $containerMap = [
            [MediaService::class, $this->getMockBuilder(MediaService::class)->disableOriginalConstructor()->getMock()]
        ];
        $container = $this->getMockBuilder(ContainerInterface::class)->getMock();
        $container->method('get')->willReturnMap($containerMap);

        $factory = new MediaHelperFactory();
        $this->assertInstanceOf(FactoryInterface::class, $factory);

        $result = $factory->__invoke($container, 'view-helper-media');
        $this->assertInstanceOf(MediaHelper::class, $result);
    }
}
