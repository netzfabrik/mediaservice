<?php
namespace MediaTest\View\Helper;

use Media\Entity\Media;
use Media\Service\MediaService;
use Media\View\Helper\MediaHelper;
use PHPUnit\Framework\TestCase;
use Zend\View\Helper\AbstractHelper;

/**
 * Tests for the MediaHelper
 * @author heik
 * @covers Media\View\Helper\Mediahelper
 */
class MediaHelperTest extends TestCase
{
    /**
     * Test inteface and invoke method
     */
    public function testInvoke()
    {

        $mediaService = $this->getMockBuilder(MediaService::class)->disableOriginalConstructor()->getMock();
        $helper = new MediaHelper($mediaService);

        $this->assertInstanceOf(AbstractHelper::class, $helper);


        $media = $this->getMockBuilder(Media::class)->getMock();
        $x = 400;
        $y = 300;

        $mediaService->expects($this->once())->method('getPublicPath')->with($media, $x, $y)->willReturn('pathToImage');
        $this->assertEquals('<img src="pathToImage" />', $helper->__invoke($media, $x, $y));
    }
}
