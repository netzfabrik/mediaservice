<?php
namespace MediaTest\Exception;

use Media\Exception\MediaException;
use PHPUnit\Framework\TestCase;

/**
 * Tests for the MediaException
 * @author heik
 * @covers Media\Exception\MediaException
 */
class MediaExceptionTest extends TestCase
{
    /**
     * Test inheritance
     */
    public function testInheritance()
    {
        $exception = new MediaException();
        $this->assertInstanceOf(\Exception::class, $exception);
    }
}
