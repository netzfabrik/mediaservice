<?php
namespace MediaTest\Exception;

use Media\Exception\FileNotReadableException;
use Media\Exception\MediaException;
use PHPUnit\Framework\TestCase;

/**
 * Tests for the FileNotReadableException
 * @author heik
 * @covers Media\Exception\FileNotReadableException
 */
class FileNotReadableExceptionTest extends TestCase
{
    /**
     * Test inheritance
     */
    public function testInheritance()
    {
        $exception = new FileNotReadableException();
        $this->assertInstanceOf(MediaException::class, $exception);
    }
}
