<?php
namespace MediaTest\Exception;

use Media\Exception\NotFoundException;
use Media\Exception\MediaException;
use PHPUnit\Framework\TestCase;

/**
 * Tests for the NotFoundException
 * @author heik
 * @covers Media\Exception\NotFoundException
 */
class NotFoundExceptionTest extends TestCase
{
    /**
     * Test inheritance
     */
    public function testInheritance()
    {
        $exception = new NotFoundException();
        $this->assertInstanceOf(MediaException::class, $exception);
    }
}
