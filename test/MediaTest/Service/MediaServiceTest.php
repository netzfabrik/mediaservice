<?php
namespace MediaTest\Service;

use Media\Entity\Media;
use Media\Exception\FileNotReadableException;
use Media\Exception\MediaException;
use Media\Exception\NotFoundException;
use Media\Repository\MediaRepository;
use Media\Service\FileHandlerService;
use Media\Service\MediaService;
use Media\Service\TransformationService;
use PHPUnit\Framework\TestCase;
use Zend\Http\PhpEnvironment\Response;
use Zend\Config\Config;

/**
 * Tests for the MediaService
 * @author heik
 * @covers Media\Service\MediaService
 */
class MediaServiceTest extends TestCase
{
    /**
     * @var MediaRepository
     */
    private $repository;

    /**
     * @var FileHandlerService
     */
    private $fileHandlerService;

    /**
     * @var TransformationService
     */
    private $transformationService;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var MediaService
     */
    private $mediaService;

    /**
     * @var string
     */
    private $tmpDir;

    /**
     * {@inheritDoc}
     * @see \PHPUnit\Framework\TestCase::setUp()
     */
    protected function setUp()
    {
        $this->tmpDir = sys_get_temp_dir() . '/unittest-mediaService';

        $this->repository = $this->getMockBuilder(MediaRepository::class)->disableOriginalConstructor()->getMock();

        $this->fileHandlerService = $this->getMockBuilder(FileHandlerService::class)->disableOriginalConstructor()->getMock();
        $this->fileHandlerService->method('getMediaPath')->willreturn($this->tmpDir . '/mediaPath');
        $this->fileHandlerService->method('getSourcePath')->willreturn($this->tmpDir . '/sourcePath');

        $this->transformationService = $this->getMockBuilder(TransformationService::class)->disableOriginalConstructor()->getMock();

        $configMap = [
            ['basePath', null, $this->tmpDir],
            ['sourcePath', null, 'sourcePath'],
            ['mediaPath', null, 'mediaPath'],
            ['basePublic', null, 'publicpath']
        ];
        $this->config = $this->getMockBuilder(Config::class)->disableOriginalConstructor()->getMock();
        $this->config->method('get')->willReturnMap($configMap);

        if (!is_dir($basePath = $this->tmpDir)) {
            mkdir($basePath, 0775, true);
        }

        if (!is_dir($sourcePath = $this->tmpDir . '/sourcePath')) {
            mkdir($sourcePath, 0775, true);
            copy(__DIR__ . '/_mock/php-logo.jpg', $sourcePath . '/php-logo.jpg');
        }

        if (!is_dir($mediaPath = $this->tmpDir . '/mediaPath')) {
            mkdir($mediaPath, 0775, true);
            copy(__DIR__ . '/_mock/php-logo.jpg', $mediaPath . '/php-logo.jpg');
        }

        $this->mediaService = new MediaService($this->repository, $this->transformationService, $this->fileHandlerService, $this->config);
    }

    /**
     * {@inheritDoc}
     * @see \PHPUnit\Framework\TestCase::tearDown()
     */
    protected function tearDown()
    {
        // clean and remove tempdir
        if (is_dir($this->tmpDir)) {
            foreach (glob($this->tmpDir . '/sourcePath/*') as $file) {
                unlink($file);
            }
            foreach (glob($this->tmpDir . '/mediaPath/*') as $file) {
                unlink($file);
            }
            rmdir($this->tmpDir . '/sourcePath');
            rmdir($this->tmpDir . '/mediaPath');
            rmdir($this->tmpDir);
        }
    }

    /**
     * Test adding a Media of type jpeg
     */
    public function testAddJpeg()
    {
        $uploadData = ['tmp_name' => __DIR__ . '/_mock/php-logo.jpg'];
        $data = ['note' => 'testCase'];

        $this->repository->expects($this->once())->method('persist')->with($this->isInstanceOf(Media::class));

        $result = $this->mediaService->add($uploadData, $data);
        $this->assertInstanceOf(Media::class, $result);
    }

    /**
     * Test adding a Media of type gif
     */
    public function testAddGif()
    {
        $uploadData = ['tmp_name' => __DIR__ . '/_mock/php-logo.gif'];
        $data = ['note' => 'testCase'];

        $this->repository->expects($this->once())->method('persist')->with($this->isInstanceOf(Media::class));

        $result = $this->mediaService->add($uploadData, $data);
        $this->assertInstanceOf(Media::class, $result);
    }

    /**
     * Test adding a Media failes - invalid mime type
     */
    public function testAddInvalidMimeType()
    {
        $this->expectException(MediaException::class);
        $this->expectExceptionMessage('Only jpeg and gif allowed.');

        $uploadData = ['tmp_name' => __DIR__ . '/_mock/php-logo.txt'];
        $data = ['note' => 'testCase'];

        $this->mediaService->add($uploadData, $data);
    }

    /**
     * Test add fails - missing tmp_name
     */
    public function testAddMissingTmpFile()
    {
        $this->expectException(MediaException::class);
        $this->expectExceptionMessage('Missing "tmp_name" in upload data.');

        $uploadData = [];
        $this->mediaService->add($uploadData);
    }

    /**
     * Test add fails - tmp_name not readable
     */
    public function testAddTmpFileNotReadable()
    {
        $this->expectException(MediaException::class);
        $this->expectExceptionMessage('Uploaded file could not be read.');

        $uploadData = ['tmp_name' => __DIR__ . '/_mock/notexists.jpg'];;
        $this->mediaService->add($uploadData);
    }

    /**
     * Test getting media response
     */
    public function testGetMedia()
    {
        $mediaId = 134;
        $x = 200;
        $y = 400;

        $media = $this->getMockBuilder(Media::class)->getMock();
        $media->expects($this->once())->method('getPath')->willReturn('php-logo.jpg');
        $media->expects($this->once())->method('getMime')->willReturn('image/jpeg');
        $media->expects($this->once())->method('getSize')->willReturn(4500);

        $mediaService = $this->getMockBuilder(MediaService::class)
            ->setConstructorArgs([$this->repository, $this->transformationService, $this->fileHandlerService, $this->config])
            ->setMethods(['getMediaById'])
            ->getMock();
        $mediaService->expects($this->once())->method('getMediaById')->with($mediaId, $x, $y)->willReturn($media);

        $result = $mediaService->getMedia($mediaId, $x, $y);
        $this->assertInstanceOf(Response::class, $result);
    }

    /**
     * test getting media reponse failes - file not readable in storage
     */
    public function testGetMediaPathInvalid()
    {
        $this->expectException(FileNotReadableException::class);

        $mediaId = 134;
        $x = 200;
        $y = 400;

        $media = $this->getMockBuilder(Media::class)->getMock();
        $media->expects($this->once())->method('getPath')->willReturn('notExists');

        $mediaService = $this->getMockBuilder(MediaService::class)
            ->setConstructorArgs([$this->repository, $this->transformationService, $this->fileHandlerService, $this->config])
            ->setMethods(['getMediaById'])
            ->getMock();
        $mediaService->expects($this->once())->method('getMediaById')->with($mediaId, $x, $y)->willReturn($media);

        $result = $mediaService->getMedia($mediaId, $x, $y);
        $this->assertInstanceOf(Response::class, $result);

        $result = $this->mediaService->getMedia($mediaId, $x, $y);
        $this->assertInstanceOf(Response::class, $result);
    }

    /**
     * Test deletion
     */
    public function testDelete()
    {
        $mediaId = 134;
        $media = $this->getMockBuilder(Media::class)->getMock();

        $mediaService = $this->getMockBuilder(MediaService::class)
            ->setConstructorArgs([$this->repository, $this->transformationService, $this->fileHandlerService, $this->config])
            ->setMethods(['getMediaById'])
            ->getMock();
        $mediaService->expects($this->once())->method('getMediaById')->with($mediaId, null, null, false)->willReturn($media);

        $this->repository->expects($this->once())->method('remove')->with($media);

        $this->assertNull($mediaService->delete($mediaId));
    }

    /**
     * Test getting media by id with matching size
     */
    public function testGetMediaById()
    {
        $mediaId = 134;
        $x = 300;
        $y = 400;

        $source = $this->getMockBuilder(Media::class)->getMock();

        $media = $this->getMockBuilder(Media::class)->getMock();
        $media->expects($this->once())->method('getSource')->willReturn($source);

        $this->repository->expects($this->once())->method('findMediaByIdAndSize')->with($mediaId, $x, $y)->willReturn($media);

        $this->assertSame($media, $this->mediaService->getMediaById($mediaId, $x, $y));
    }

    /**
     * Get media by id with different sizes - resize
     */
    public function testGetMediaByIdWithResize()
    {
        $mediaId = 134;
        $x = 300;
        $y = 400;

        $media = $this->getMockBuilder(Media::class)->getMock();
        $media2 = $this->getMockBuilder(Media::class)->getMock();

        $mediaService = $this->getMockBuilder(MediaService::class)
            ->setConstructorArgs([$this->repository, $this->transformationService, $this->fileHandlerService, $this->config])
            ->setMethods(['createResizedMedia'])
            ->getMock();
        $mediaService->expects($this->once())->method('createResizedMedia')->with($mediaId, $x, $y)->willReturn($media2);

        $this->repository->expects($this->once())->method('findMediaByIdAndSize')->with($mediaId, $x, $y)->willReturn(null);

        $this->assertSame($media2, $mediaService->getMediaById($mediaId, $x, $y));
    }

    /**
     * Media by id fails - not found
     */
    public function testGetMediaByIdNotFound()
    {
        $this->expectException(NotFoundException::class);

        $mediaId = 134;
        $x = 300;
        $y = 400;

        $mediaService = $this->getMockBuilder(MediaService::class)
            ->setConstructorArgs([$this->repository, $this->transformationService, $this->fileHandlerService, $this->config])
            ->setMethods(['createResizedMedia'])
            ->getMock();
        $mediaService->expects($this->once())->method('createResizedMedia')->with($mediaId, $x, $y)->willReturn(null);
        $this->repository->expects($this->once())->method('findMediaByIdAndSize')->with($mediaId, $x, $y)->willReturn(null);

        $mediaService->getMediaById($mediaId, $x, $y);
    }

    /**
     * NotFound Exception is raised, when source file not present
     */
    public function testCreateResizedMediaNotFound()
    {
        $this->expectException(NotFoundException::class);

        $mediaId = 134;
        $x = 300;
        $y = 400;

        $this->repository->expects($this->once())->method('find')->with($mediaId)->willReturn(null);

        $this->mediaService->createResizedMedia($mediaId, $x, $y);
    }

    /**
     * Test create a resized Media entity
     */
    public function testCreateResized()
    {
        $mediaId = 134;
        $x = 300;
        $y = 400;

        $media = $this->getMockBuilder(Media::class)->getMock();
        $media->method('getSource')->willReturn(null);
        $media->method('getPath')->willReturn('php-logo.jpg');

        $this->repository->expects($this->once())->method('find')->with($mediaId)->willReturn($media);
        $this->repository->expects($this->once())->method('persist')->with($this->isInstanceOf(Media::class));

        $this->transformationService->expects($this->once())->method('transform')
            ->willReturn($this->fileHandlerService->getSourcePath() . '/php-logo.jpg');

        $result = $this->mediaService->createResizedMedia($mediaId, $x, $y);
        $this->assertInstanceOf(Media::class, $result);
    }

    /**
     * Test create a resized Media entity using source
     */
    public function testCreateResizedWithSource()
    {
        $mediaId = 134;
        $mediaSourceId = 124;     
           
        $x = 300;
        $y = 400;

        $source = $this->getMockBuilder(Media::class)->getMock();
        $source->method('getPath')->willReturn('php-logo.jpg');
    	$source->method('getId')->willReturn($mediaSourceId);
		$source->method('getSource')->willReturn(null);	

        $media = $this->getMockBuilder(Media::class)->getMock();
        $media->method('getSource')->willReturn($source);

		$findMap = [
			[$mediaId, null, null, $media],
			[$mediaSourceId, null, null, $source]
		];
        $this->repository->method('find')->willReturnMap($findMap);
        $this->repository->expects($this->once())->method('persist')->with($this->isInstanceOf(Media::class));

        $this->transformationService->expects($this->once())->method('transform')
            ->willReturn($this->fileHandlerService->getSourcePath() . '/php-logo.jpg');

        $result = $this->mediaService->createResizedMedia($mediaId, $x, $y);
        $this->assertInstanceOf(Media::class, $result);
        $this->assertSame($source, $result->getSource());
    }

    /**
     * Test generate public path
     */
    public function testGetPublicPath()
    {
        $mediaId = 123;
        $media = $this->getMockBuilder(Media::class)->getMock();
        $media->expects($this->once())->method('getId')->willReturn($mediaId);
        $media->expects($this->once())->method('getPath')->willReturn('pathToImage');

        $x = 300;
        $y = 400;

        $mediaService = $this->getMockBuilder(MediaService::class)
            ->setConstructorArgs([$this->repository, $this->transformationService, $this->fileHandlerService, $this->config])
            ->setMethods(['getMediaById'])
            ->getMock();
        $mediaService->expects($this->once())->method('getMediaById')->with($mediaId, $x, $y)->willReturn($media);

        $result = $mediaService->getPublicPath($media, $x, $y);
        $this->assertEquals('publicpath/pathToImage', $result);
    }
}
