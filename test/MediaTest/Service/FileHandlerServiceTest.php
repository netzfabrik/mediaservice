<?php
namespace {
    $mkDir = true;
    $copy = true;
    $isDir = false;
}

namespace Media\Service {

    /**
     * Override mkdir function
     */
    function mkdir($pathname, $mode = null, $recursive = null, $context = null) {
        global $mkDir;
        return $mkDir;
    }

    /**
     * Override copy function
     */
    function copy($source, $dest, $context = null) {
        global $copy;
        return $copy;
    }

    /**
     * Override is_dir function
     */
    function is_dir($filename) {
        global $isDir;
        return $isDir;
    }

    /**
     * Override realpath function
     */
    function realpath($targetDir)
    {
        return $targetDir;
    }
}

namespace MediaTest\Service {

    use PHPUnit\Framework\TestCase;
    use Zend\Config\Config;
    use Media\Service\FileHandlerService;
    use Media\Exception\MediaException;

    /**
     * Tests for the FileHandlerService
     * @author heik
     * @covers Media\Service\FileHandlerService
     */
    class FileHandlerServiceTest extends TestCase
    {
        /**
         * @var Config
         */
        private $config;

        /**
         * @var string
         */
        private $tempDir;

        /**
         * {@inheritDoc}
         * @see \PHPUnit\Framework\TestCase::setUp()
         */
        protected function setUp()
        {
            // restore defaults
            global $copy, $mkDir, $isDir;
            $mkdir = $copy = true;
            $isDir = false;

            $this->tempDir = sys_get_temp_dir() . '/unittest-filehandlerService/';

            $this->config = $this->getMockBuilder(Config::class)->disableOriginalConstructor()->getMock();
            $configMap = [
                ['basePath', null, $this->tempDir],
                ['sourcePath', null, 'sourcePath'],
                ['mediaPath', null, 'mediaPath'],
            ];
            $this->config->method('get')->willReturnMap($configMap);
        }

        /**
         * {@inheritDoc}
         * @see \PHPUnit\Framework\TestCase::tearDown()
         */
        protected function tearDown()
        {
            // clean and remove tempdir
            if (is_dir($this->tempDir)) {
                foreach (glob($this->tempDir . '/*') as $file) {
                    unlink($file);
                }
                rmdir($this->tempDir);
            }
        }

        /**
         * Test copy file - including creation of target dir
         */
        public function testCopyFileWithCreate()
        {
            $source = __DIR__ . '/_mock/php-logo.jpg';
            $destination = $this->tempDir . '/filehandlerServiceTest.jpg';
            $service = new FileHandlerService($this->config);

            $this->assertNull($service->copyFile($source, $destination));
        }

        /**
         * Test copy file - target dir exists
         */
        public function testCopyFileTargetDirExists()
        {
            global $isDir;
            $isDir = true;

            $source = __DIR__ . '/_mock/php-logo.jpg';
            $destination = __DIR__ . '/_mock/targetFilehandlerServiceTest.jpg';
            $service = new FileHandlerService($this->config);

            $this->assertNull($service->copyFile($source, $destination));
        }

        /**
         * Test copy file fails
         */
        public function testCopyFileFails()
        {
            global $copy;
            $copy = false;

            $this->expectException(MediaException::class);
            $this->expectExceptionMessage('File could not be copied.');

            $source = __DIR__ . '/_mock/php-logo.jpg';
            $destination = $this->tempDir . '/filehandlerServiceTest.jpg';
            $service = new FileHandlerService($this->config);

            $this->assertNull($service->copyFile($source, $destination));
            $this->assertTrue(is_file($destination));
        }

        /**
         * Test copy fails - source file not readable
         */
        public function testCopyFileSourceInvalid()
        {
            $this->expectException(MediaException::class);
            $this->expectExceptionMessage('Source file could not be read.');

            $source = __DIR__ . '/_mock/notexist.jpg';
            $destination = $this->tempDir . '/filehandlerServiceTest.jpg';
            $service = new FileHandlerService($this->config);

            $service->copyFile($source, $destination);
        }

        /**
         * Test copy fails - destination path could not be created
         */
        public function testCopyFileCreateDirFailes()
        {
            global $mkDir;
            $mkDir = false;

            $this->expectException(MediaException::class);
            $this->expectExceptionMessage('Media target directory could not be created.');

            $source = __DIR__ . '/_mock/php-logo.jpg';
            $destination = $this->tempDir . '/filehandlerServiceTest.jpg';
            $service = new FileHandlerService($this->config);

            $service->copyFile($source, $destination);
        }

        /**
         * Test getting source path - including creation
         */
        public function testGetSourcePathWithCreate()
        {
            global $isDir, $mkDir;
            $isDir = false;
            $mkDir = true;

            $service = new FileHandlerService($this->config);
            $this->assertEquals($this->tempDir . 'sourcePath', $service->getSourcePath());
        }

        /**
         * Test getting source path - including creation
         */
        public function testGetSourcePath()
        {
            global $isDir;
            $isDir = true;

            $service = new FileHandlerService($this->config);
            $this->assertEquals($this->tempDir . 'sourcePath', $service->getSourcePath());
        }

        /**
         * Test getting media path - including creation
         */
        public function testGetMediaPathWithCreate()
        {
            global $isDir, $mkDir;
            $isDir = false;
            $mkDir = true;

            $service = new FileHandlerService($this->config);
            $this->assertEquals($this->tempDir . 'mediaPath', $service->getMediaPath());
        }

        /**
         * Test getting media path - including creation
         */
        public function testGetMediaPath()
        {
            global $isDir;
            $isDir = true;

            $service = new FileHandlerService($this->config);
            $this->assertEquals($this->tempDir . 'mediaPath', $service->getMediaPath());
        }
    }
}
