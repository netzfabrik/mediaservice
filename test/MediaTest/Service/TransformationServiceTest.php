<?php
namespace MediaTest\Service;

use Media\Exception\FileNotReadableException;
use Media\Exception\MediaException;
use Media\Service\TransformationService;
use PHPUnit\Framework\TestCase;

/**
 * Tests for the TransformationService
 * @author heik
 * @covers Media\Service\TransformationService
 */
class TransformationServiceTest extends TestCase
{
    /**
     * Test exception is thrown if source file not readable
     */
    public function testTransformNotReadable()
    {
        $this->expectException(FileNotReadableException::class);

        $service = new TransformationService();
        $service->transform(__DIR__ . '/_mock/notexists.jpg');
    }

    /**
     * Test exception is thrown for upscale
     */
    public function testTransformUpscaleWithAutosizeFalse()
    {
        $this->expectException(MediaException::class);
        $this->expectExceptionMessage('Upscaling images leads to problems.');

        $service = new TransformationService();
        $service->transform(__DIR__ . '/_mock/php-logo.jpg', 1000, 1000, 'jpeg', false);
    }

    /**
     * Test resize X
     */
    public function testTransformX()
    {
        $service = new TransformationService();
        $this->assertInternalType('string', $service->transform(__DIR__ . '/_mock/php-logo.jpg', 100));
    }

    /**
     * Test resize Y
     */
    public function testTransformY()
    {
        $service = new TransformationService();
        $this->assertInternalType('string', $service->transform(__DIR__ . '/_mock/php-logo.jpg', null, 100));
    }

    /**
     * Test resize X + Y
     */
    public function testTransformXY()
    {
        $service = new TransformationService();
        $this->assertInternalType('string', $service->transform(__DIR__ . '/_mock/php-logo.jpg', 100, 100));
    }
}
