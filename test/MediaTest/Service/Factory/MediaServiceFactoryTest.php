<?php
namespace MediaTest\Repository\Factory;

use Interop\Container\ContainerInterface;
use Media\Repository\MediaRepository;
use Media\Service\FileHandlerService;
use Media\Service\MediaService;
use Media\Service\TransformationService;
use Media\Service\Factory\MediaServiceFactory;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Test for the MediaServiceFactory
 * @author heik
 * @covers Media\Service\Factory\MediaServiceFactory
 */
class MediaServiceFactoryTest extends TestCase
{
    /**
     * Test creation of service
     */
    public function testCreateService()
    {
        $containerMap = [
            ['Config', ['service_media' => []]],
            [MediaRepository::class, $this->getMockBuilder(MediaRepository::class)->disableOriginalConstructor()->getMock()],
            [TransformationService::class, $this->getMockBuilder(TransformationService::class)->disableOriginalConstructor()->getMock()],
            [FileHandlerService::class, $this->getMockBuilder(FileHandlerService::class)->disableOriginalConstructor()->getMock()],
        ];

        $container = $this->getMockBuilder(ContainerInterface::class)->getMock();
        $container->method('get')->willReturnMap($containerMap);

        $factory = new MediaServiceFactory();
        $this->assertInstanceOf(FactoryInterface::class, $factory);

        $result = $factory->__invoke($container, 'media-service');
        $this->assertInstanceOf(MediaService::class, $result);
    }
}
