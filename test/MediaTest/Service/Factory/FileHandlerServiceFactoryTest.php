<?php
namespace MediaTest\Repository\Factory;

use Interop\Container\ContainerInterface;
use Media\Service\FileHandlerService;
use Media\Service\Factory\FileHandlerServiceFactory;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Test for the FileHandlerServiceFactory
 * @author heik
 * @covers Media\Service\Factory\FileHandlerServiceFactory
 */
class FileHandlerServiceFactoryTest extends TestCase
{
    /**
     * Test creation of service
     */
    public function testCreateService()
    {
        $config = [
            'service_media' => []
        ];
        $containerMap = [
            ['Config', $config]
        ];

        $container = $this->getMockBuilder(ContainerInterface::class)->getMock();
        $container->method('get')->willReturnMap($containerMap);

        $factory = new FileHandlerServiceFactory();
        $this->assertInstanceOf(FactoryInterface::class, $factory);

        $result = $factory->__invoke($container, 'filehandler-service');
        $this->assertInstanceOf(FileHandlerService::class, $result);
    }
}
