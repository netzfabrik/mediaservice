<?php
namespace MediaTest\Repository\Factory;

use Interop\Container\ContainerInterface;
use Media\Service\TransformationService;
use Media\Service\Factory\TransformationServiceFactory;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Test for the TransformationServiceFactory
 * @author heik
 * @covers Media\Service\Factory\TransformationServiceFactory
 */
class TransformationServiceFactoryTest extends TestCase
{
    /**
     * Test creation of service
     */
    public function testCreateService()
    {
        $container = $this->getMockBuilder(ContainerInterface::class)->getMock();

        $factory = new TransformationServiceFactory();
        $this->assertInstanceOf(FactoryInterface::class, $factory);

        $result = $factory->__invoke($container, 'transformation-service');
        $this->assertInstanceOf(TransformationService::class, $result);
    }
}
