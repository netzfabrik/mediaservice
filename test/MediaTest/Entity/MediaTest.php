<?php
namespace MediaTest\Entity;

use Media\Entity\Media;
use PHPUnit\Framework\TestCase;

/**
 * Tests for the media entity
 * @author heik
 * @covers Media\Entity\Media
 */
class MediaTest extends TestCase
{
    public function testGetterSetter()
    {
        $media = new Media();

        $this->assertNull($media->setId(2));
        $this->assertEquals(2, $media->getId());

        $this->assertNull($media->setMime('mime'));
        $this->assertEquals('mime', $media->getMime());

        $this->assertNull($media->setNote('note'));
        $this->assertEquals('note', $media->getNote());

        $this->assertNull($media->setPath('path'));
        $this->assertEquals('path', $media->getPath());

        $this->assertNull($media->setSize(1234));
        $this->assertEquals(1234, $media->getSize());

        $source = $this->getMockBuilder(Media::class)->getMock();
        $this->assertNull($media->setSource($source));
        $this->assertSame($source, $media->getSource());

        $this->assertNull($media->setX(200));
        $this->assertEquals(200, $media->getX());

        $this->assertNull($media->setY(300));
        $this->assertEquals(300, $media->getY());

        $this->assertNull($media->setXScale(500));
        $this->assertEquals(500, $media->getXScale());

        $this->assertNull($media->setYScale(600));
        $this->assertEquals(600, $media->getYScale());        	
    }
}
