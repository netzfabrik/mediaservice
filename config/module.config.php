<?php
namespace Media;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;

return [
    'service_manager' => [
        'factories' => [
            Repository\MediaRepository::class => Repository\Factory\MediaRepositoryFactory::class,
            Service\MediaService::class => Service\Factory\MediaServiceFactory::class,
            Service\FileHandlerService::class => Service\Factory\FileHandlerServiceFactory::class,
            Service\TransformationService::class => Service\Factory\TransformationServiceFactory::class,
        ]
    ],

    'service_media' => [
        'basePath' => sys_get_temp_dir() . '/media/',
        'sourcePath' => 'source',
        'mediaPath' => 'media',
        'basePublic' => '/'
    ],

    'view_helpers' => [
        'factories' => [
            View\Helper\MediaHelper::class => View\Helper\Factory\MediaHelperFactory::class
        ],
        'aliases' => [
            'media' => View\Helper\MediaHelper::class
        ]
    ],

    'doctrine' => [
        'driver' => [
            'mediaservice_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity',
                ],
            ],
            'orm_default' => [
                'drivers' => [
                    'Media\\Entity' => 'mediaservice_driver'
                ],
            ],
        ],
    ]
];
