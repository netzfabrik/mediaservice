<?php
namespace Media\Repository;

use Doctrine\ORM\EntityRepository;
use Media\Entity\Media;

/**
 * Repository for Media entities
 */
class MediaRepository extends EntityRepository
{
    /**
     * Find a Media by id and given dimension
     * @param int $mediaId
     * @param int $x
     * @param int $y
     * @return Media
     */
    public function findMediaByIdAndSize($mediaId, $x = null, $y = null)
    {
        if (null !== $x && null !== $y) {
            $media = $this->findOneBy(['source' => $mediaId, 'xScale' => $x, 'yScale' => $y]);
        } elseif (null !== $x) {
            $media = $this->findOneBy(['source' => $mediaId, 'xScale' => $x]);
        } elseif (null !== $y) {
            $media = $this->findOneBy(['source' => $mediaId, 'yScale' => $y]);
        } else {
            $media = $this->find($mediaId);
        }
        return $media;
    }

    /**
     * Remove a media entry
     * @param Media $media
     */
    public function remove(Media $media)
    {
        $em = $this->getEntityManager();
        $em->remove($media);
        $em->flush($media);
    }

    /**
     * Persist Media entity
     * @param Media $entity
     * @param boolean $flush
     * @return Media
     */
    public function persist(Media $entity, $flush = true)
    {
        $em = $this->getEntityManager();
        $em->persist($entity);
        if ($flush) {
            $em->flush($entity);
        }
        return $entity;
    }
}
