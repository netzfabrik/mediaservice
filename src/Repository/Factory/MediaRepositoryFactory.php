<?php
namespace Media\Repository\Factory;

use Doctrine\ORM\Mapping\ClassMetadata;
use Interop\Container\ContainerInterface;
use Media\Entity\Media;
use Media\Repository\MediaRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Factory for the MediaRepository
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @author heik
 */
class MediaRepositoryFactory implements FactoryInterface
{
    /**
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $classMeta = new ClassMetadata(Media::class);
        return new MediaRepository($entityManager, $classMeta);
    }
}
