<?php
namespace Media\Exception;

/**
 * Media could not be found exception
 */
class NotFoundException extends MediaException
{
    /**
     * @param string $message
     * @param number $code
     */
    public function __construct($message = 'Requested media could not be found', $code = 404)
    {
        parent::__construct($message, $code);
    }
}
