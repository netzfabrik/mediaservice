<?php
namespace Media\Exception;

/**
 * Media file could not be read exception
 */
class FileNotReadableException extends MediaException
{
    /**
     * @param string $message
     * @param number $code
     */
    public function __construct($message = 'Requested media could not be found in storage', $code = 404)
    {
        parent::__construct($message, $code);
    }
}
