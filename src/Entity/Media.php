<?php
namespace Media\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="media")
 */
class Media
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Media")
     * @var Media
     */
    private $source;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $x;

    /**
     * @ORM\Column(type="integer")
     * @var int
     */
    private $y;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int
     */
    private $xScale;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var int
     */
    private $yScale;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $mime;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $size;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $path;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @var string
     */
    private $note = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Media
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param Media $source
     */
    public function setSource(Media $source)
    {
        $this->source = $source;
    }

    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param int $x
     */
    public function setX($x)
    {
        $this->x = $x;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param int $y
     */
    public function setY($y)
    {
        $this->y = $y;
    }

    /**
     * @return int
     */
    public function getXScale()
    {
        return $this->xScale;
    }

    /**
     * @param int $xScale
     */
    public function setXScale($xScale)
    {
        $this->xScale = $xScale;
    }

    /**
     * @return int
     */
    public function getYScale()
    {
        return $this->yScale;
    }

    /**
     * @param int $yScale
     */
    public function setYScale($yScale)
    {
        $this->yScale = $yScale;
    }

    /**
     * @return string
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * @param string $mime
     */
    public function setMime($mime)
    {
        $this->mime = $mime;
    }

    /**
     * @return string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param string $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }
}
