<?php
namespace Media\Service;

use Media\Exception\FileNotReadableException;
use Media\Exception\MediaException;

/**
 * Image transformation Service
 */
class TransformationService
{
    /**
     * Transform given source file to given dimensions and return new (tmp) path. If autoSize is set false,
     * an exception is thrown on upscaling images. If true (default), the source images original dimensions
     * are taken.
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @param string $sourceMediaPath
     * @param int $width
     * @param int $height
     * @param string $imageFormat
     * @param boolean $autoSize
     * @return string
     * @throws MediaException
     */
    public function transform($sourceMediaPath, $width = null, $height = null, $imageFormat = 'jpeg', $autoSize = true)
    {
        // no source found
        if (!is_readable($sourceMediaPath)) {
            throw new FileNotReadableException();
        }

        $filehandle = fopen($sourceMediaPath, 'r');
        $im = new \Imagick();
        $im->readimagefile($filehandle);

        // image original size
        $oWidth = $im->getimagewidth();
        $oHeight = $im->getimageheight();

        if ($width > $oWidth || $height > $oHeight) {
            if (!$autoSize) {
                throw new MediaException('Upscaling images leads to problems.');
            }
        }

        // only one dimension given
        if (!$width || !$height) {

            $width = min($oWidth, $width);
            $height = min($oHeight, $height);

            if ($width && !$height) {
                $factor = $oHeight / $oWidth;
                $height = $width / $factor;
            }

            if ($height && !$width) {
                $factor = $oWidth / $oHeight;
                $width = $height * $factor;
            }
        }

        $im->setimageformat($imageFormat);
        $im->setCompression(\Imagick::COMPRESSION_LOSSLESSJPEG);
        $im->setimagetype(\Imagick::IMGTYPE_TRUECOLOR);
        $im->setimagecompressionquality(100);

        $scale = (($height && $height != $oHeight) || ($width && $width != $oWidth));
        if ($scale) {
            $im->scaleImage($width, $height, true);
            $im->unsharpMaskImage(0, 1, 1, 0.05);
        }

        $imagePath = tempnam(sys_get_temp_dir(), 'media-resized');
        $im->writeimage($imagePath);

        return $imagePath;
    }
}
