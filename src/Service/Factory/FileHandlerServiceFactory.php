<?php
namespace Media\Service\Factory;

use Interop\Container\ContainerInterface;
use Media\Service\FileHandlerService;
use Zend\Config\Config;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Factory for the FileHandlerService
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class FileHandlerServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        $configuration = new Config($config['service_media']);
        return new FileHandlerService($configuration);
    }
}
