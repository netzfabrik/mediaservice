<?php
namespace Media\Service\Factory;

use Interop\Container\ContainerInterface;
use Media\Repository\MediaRepository;
use Media\Service\FileHandlerService;
use Media\Service\MediaService;
use Media\Service\TransformationService;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Config\Config;

/**
 * Factory for the MediaService
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @author heik
 */
class MediaServiceFactory implements FactoryInterface
{
    /**
     * (non-PHPdoc)
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $repository = $container->get(MediaRepository::class);
        $transformationService = $container->get(TransformationService::class);
        $fileHandlerService = $container->get(FileHandlerService::class);

        $config = $container->get('Config');
        $configuration = new Config($config['service_media']);

        return new MediaService($repository, $transformationService, $fileHandlerService, $configuration);
    }
}
