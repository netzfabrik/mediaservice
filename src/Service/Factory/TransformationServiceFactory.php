<?php
namespace Media\Service\Factory;

use Media\Service\TransformationService;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Factory for the ImageTransformationService
 * @SuppressWarnings(PHPMD.UnusedLocalVariable)
 */
class TransformationServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new TransformationService();
    }
}
