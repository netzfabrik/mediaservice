<?php
namespace Media\Service;

use Media\Entity\Media;
use Media\Exception\FileNotReadableException;
use Media\Exception\MediaException;
use Media\Exception\NotFoundException;
use Media\Repository\MediaRepository;
use Zend\Config\Config;
use Zend\Http\Headers;
use Zend\Http\PhpEnvironment\Response;

/**
 * Media service
 */
class MediaService
{
    /**
     * @var MediaRepository
     */
    private $repository;

    /**
     * @var FileHandlerService
     */
    private $fileHandlerService;

    /**
     * @var TransformationService
     */
    private $transformationService;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param MediaRepository $repository
     * @param TransformationService $transformationService
     * @param FileHandlerService $fileHandlerService
     * @param Config $config
     */
    public function __construct(
        MediaRepository $repository,
        TransformationService $transformationService,
        FileHandlerService $fileHandlerService,
        Config $config
    ) {
        $this->repository = $repository;
        $this->transformationService = $transformationService;
        $this->fileHandlerService = $fileHandlerService;
        $this->config = $config;
    }

    /**
     * Add new Media from given upload data with optional additional data
     * @param array $uploadData
     * @param array $data
     * @return Media
     */
    public function add(array $uploadData, array $data = [])
    {
        if (!isset($uploadData['tmp_name'])) {
            throw new MediaException('Missing "tmp_name" in upload data.');
        }

        $file = $uploadData['tmp_name'];
        if (!is_readable($file)) {
            throw new MediaException('Uploaded file could not be read.');
        }

        $media = $this->moveFileAndCreateMediaEntity($file);

        $note = isset($data['note']) ? $data['note'] : null;
        $media->setNote($note);

        $this->repository->persist($media);
        return $media;
    }

    /**
     * Get media as Response object including headers
     * @param int $mediaId
     * @param int $x
     * @param int $y
     * @throws NotFoundException
     * @return Response
     */
    public function getMedia($mediaId, $x = null, $y = null)
    {
        $media = $this->getMediaById($mediaId, $x, $y);
        $mediaPath = $this->getMediaStoragePath($media);
        if (!is_readable($mediaPath)) {
            throw new FileNotReadableException();
        }

        $headers = new Headers();
        $headers->addHeaders([
            'Content-Type' => $media->getMime(),
            'Content-Length' => $media->getSize()
        ]);

        $response = new Response();
        $response->setContent(file_get_contents($mediaPath));
        $response->setHeaders($headers);

        return $response;
    }

    /**
     * Delete media entity from repository
     * @param int $mediaId
     * @return void
     */
    public function delete($mediaId)
    {
        $media = $this->getMediaById($mediaId, null, null, false);
        if ($media) {
            $this->repository->remove($media);
        }
    }

    /**
     * Get Media entity by id with given dimensions
     * @param int $mediaId
     * @param int $x
     * @param int $y
     * @param bool $resize
     * @throws NotFoundException
     * @throws FileNotReadableException
     * @return Media
     */
    public function getMediaById($mediaId, $x = null, $y = null, $resize = true)
    {
        $media = $this->repository->findMediaByIdAndSize($mediaId, $x, $y);
        if ((!$media || !$media->getSource()) && $resize) {
            $media = $this->createResizedMedia($mediaId, $x, $y);
        }

        if (!$media) {
            // entity not found
            throw new NotFoundException();
        }

        return $media;
    }

    /**
     * Resize given media by id and return new Media entity
     * @param int $mediaId
     * @param int $x
     * @param int $y
     * @return Media
     */
    public function createResizedMedia($mediaId, $x = null, $y = null)
    {
        /* @var $sourceMedia \Media\Entity\Media */
        $sourceMedia = $this->repository->find($mediaId);
        if (!$sourceMedia) {
            throw new NotFoundException();
        }

        // use source for transformation - results in best quality
        if ($sourceMedia->getSource() instanceof Media) {
            $sourceMedia = $sourceMedia->getSource();
            return $this->createResizedMedia($sourceMedia->getId(), $x, $y);
        }
        $sourceMediaPath = $this->getMediaStoragePath($sourceMedia);

        // resisze media and create new image file
        $newSourceFile = $this->transformationService->transform($sourceMediaPath, $x, $y);

        // move new image file into media storage and create Media entity
        $media = $this->moveFileAndCreateMediaEntity($newSourceFile, $sourceMedia);
        $media->setXScale($x);
        $media->setYScale($y);
        $media->setNote(sprintf('Resized %s : x => %s - y => %s', $sourceMedia->getId(), $x, $y));

        $this->repository->persist($media);
        return $media;
    }

    /**
     * Get public path for Media
     * @return string
     */
    public function getPublicPath(Media $media, $x = null, $y = null)
    {
        $media = $this->getMediaById($media->getId(), $x, $y);
        return sprintf('%s/%s', $this->config->get('basePublic'), $media->getPath());
    }

    /**
     * Move file and create a new media entity. Attach $source media if given
     * @param string $newSourceFile
     * @param Media $source
     * @throws MediaException
     * @return Media
     */
    private function moveFileAndCreateMediaEntity($newSourceFile, Media $source = null)
    {
        $dimensions = @getimagesize($newSourceFile);
        $mime = $dimensions['mime'];
        switch($mime) {
            case 'image/jpeg':
                $extension = 'jpg';
                break;
            case 'image/gif':
                $extension = 'gif';
                break;
            default:
                throw new MediaException('Only jpeg and gif allowed.');
        }

        $filename = sprintf('%s.%s', md5($newSourceFile), $extension);

        $rand = str_split(substr(md5(time() . $newSourceFile), 0, 16), 4);
        $storagePath = implode('/', $rand);
        $storePath = sprintf('%s/%s', $storagePath, $filename);

        $size = filesize($newSourceFile);
        $x = $dimensions[0];
        $y = $dimensions[1];

        $media = new Media();
        $media->setMime($mime);
        $media->setSize($size);
        $media->setX($x);
        $media->setY($y);
        $media->setPath($storePath);
        if ($source instanceof Media) {
            $media->setSource($source);
        }

        $storagePath = $this->getMediaStoragePath($media);
        $this->fileHandlerService->copyFile($newSourceFile, $storagePath);

        return $media;
    }

    /**
     * Determine storage path for given Media entity
     * @param Media $media
     * @return string
     */
    private function getMediaStoragePath(Media $media)
    {
        $typePath = $media->getSource() ? $this->config->get('mediaPath') : $this->config->get('sourcePath');
        return sprintf('%s/%s/%s', $this->config->get('basePath'), $typePath, $media->getPath());
    }
}
