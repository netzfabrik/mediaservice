<?php
namespace Media\Service;

use Media\Exception\MediaException;
use Zend\Config\Config;

/**
 * Service for media file handling
 */
class FileHandlerService
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Copy file from $source to $destination
     * @param string $source
     * @param string $destination
     * @throws MediaException
     */
    public function copyFile($source, $destination)
    {
        if (!is_readable($source)) {
            throw new MediaException('Source file could not be read.');
        }

        $targetDir = pathinfo($destination, PATHINFO_DIRNAME);
        $this->checkAndCreateDirectory($targetDir, 'Media target directory could not be created.');

        if (!@copy($source, $destination)) {
            throw new MediaException('File could not be copied.');
        }
    }

    /**
     * Get path to source files
     * @throws MediaException
     * @return string
     */
    public function getSourcePath()
    {
        return $this->prepareAndGetStoragePath('sourcePath');
    }

    /**
     * Get path to resized media
     * @throws MediaException
     * @return string
     */
    public function getMediaPath()
    {
        return $this->prepareAndGetStoragePath('mediaPath');
    }

    /**
     * Determine storage path for given identifier
     * @param string $identifier
     * @throws MediaException
     * @return string
     */
    private function prepareAndGetStoragePath($identifier)
    {
        $mediaPath = sprintf('%s%s', $this->config->get('basePath'), $this->config->get($identifier));
        return $this->checkAndCreateDirectory($mediaPath, "Path for $identifier could not be created.");
    }

    /**
     * Check if target directory exists and try to create it if needed.
     * @param string $targetDir
     * @param string $message
     * @throws MediaException
     * @return string
     */
    private function checkAndCreateDirectory($targetDir, $message = 'Target directory could not be created.')
    {
        if (!is_dir($targetDir)) {
            if (!@mkdir($targetDir, 0755, true)) {
                throw new MediaException($message);
            }
        }
        return realpath($targetDir);
    }
}
