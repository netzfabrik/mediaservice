<?php
namespace Media\View\Helper\Factory;

use Interop\Container\ContainerInterface;
use Media\Service\MediaService;
use Media\View\Helper\MediaHelper;
use Zend\ServiceManager\Factory\FactoryInterface;

/**
 * Factory for the MediaHelper
 * @author heik
 */
class MediaHelperFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     * @see \Zend\ServiceManager\Factory\FactoryInterface::__invoke()
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $mediaService = $container->get(MediaService::class);
        return new MediaHelper($mediaService);
    }
}
