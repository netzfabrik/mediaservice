<?php
namespace Media\View\Helper;

use Media\Entity\Media;
use Media\Service\MediaService;
use Zend\View\Helper\AbstractHelper;

/**
 * Create html img tag for Media entities. Uses MediaService to fetch
 * a resiszed image
 * @author heik
 */
class MediaHelper extends AbstractHelper
{
    /**
     * @var MediaService
     */
    private $mediaService;

    /**
     * @param MediaService $mediaService
     * @param string $basePublic
     */
    public function __construct(MediaService $mediaService)
    {
        $this->mediaService = $mediaService;
    }

    /**
     * Create an HTML img tag for Media entity with optional given
     * dimesions and title attribute
     * @param Media $media
     * @param int $x
     * @param int $y
     * @param string $imgTitle
     * @return string
     */
    public function __invoke(Media $media, $x = null, $y = null, $imgTitle = null)
    {
        $title = $imgTitle ? sprintf('title="%s" ', $imgTitle) : null;
        $src = $this->mediaService->getPublicPath($media, $x, $y);
        return sprintf(
            '<img %ssrc="%s" />',
            $title,
            $src
        );
    }
}
